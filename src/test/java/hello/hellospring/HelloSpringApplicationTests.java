package hello.hellospring;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled("test 제외")
@SpringBootTest
class HelloSpringApplicationTests {

	@Test
	void contextLoads() {
	}

}
